# QUESTION 2 - SAMPLE BUG REPORT

## PROBLEM
Error occurs when submitting the Book Appointment form

## STEPS TO REPRODUCE
1. Go to https://dev.mksp.co
2. Fill in the form on the home page and submit
3. Select a plan to continue with
4. Fill in all sections of the Book Appointment form including dummy card details for Stripe API
5. Submit the form

## ACTUAL RESULTS
An error is returned which reads "Sorry, there was an unusual error. Please try again, or feel free to give us a call!"

## EXPECTED RESULTS
Appointment is booked successfully

## ADDITIONAL INFO
Error seems to occur when sending the request to Stripe's API endpoint



from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
import unittest
import HtmlTestRunner


class TestQuestion3(unittest.TestCase):
    pass

    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        self.driver.maximize_window()
        self.driver.implicitly_wait(5)

    def test_question_3_script(self):
        "User should only be able to login with valid credentials"
        self.driver.get("https://www.daily-harvest.com/")

        login_link = WebDriverWait(self.driver, 10).until(
            expected_conditions.element_to_be_clickable((By.XPATH, "//a[@href='/login']"))
        )
        login_link.click()

        self.__test_invalid_login("", "")
        self.__test_invalid_login("user", "")
        self.__test_invalid_login("", "pass")
        self.__test_invalid_login("user", "pass")

    def __test_invalid_login(self, email, password):
        email_field = self.driver.find_element_by_id("email")
        email_field.clear()
        email_field.send_keys(email)

        password_field = self.driver.find_element_by_id("password")
        password_field.clear()
        password_field.send_keys(password)

        login_button = self.driver.find_element_by_xpath("//button[@type='submit']")
        login_button.click()

        error_message = WebDriverWait(self.driver, 10).until(
            expected_conditions.presence_of_element_located((By.XPATH, "//div[@role='alert']"))
        )

        assert "Error: Invalid login credentials" in error_message.text

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()

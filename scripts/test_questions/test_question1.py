from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import unittest


class TestQuestion1(unittest.TestCase):
    pass

    def setUp(self):
        self.driver = webdriver.Chrome(ChromeDriverManager().install())
        self.driver.maximize_window()

    def test_question_1_script(self):
        """User should be able to navigate to About Lenders page"""
        self.driver.get("https://hifiona.com")
        learn_button = self.driver.find_element_by_link_text("Learn")
        learn_button.click()
        about_lenders_button = self.driver.find_element_by_partial_link_text("About Lenders")
        about_lenders_button.click()
        expected_url = "https://hifiona.com/learn/about-lenders"
        actual_url = self.driver.current_url
        assert expected_url == actual_url

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()



# Instructions

## Python & Pip Setup

1. Download Python 3.8 from https://www.python.org/downloads/
2. Follow the Install wizard and install Python 3.8
3. Open a Terminal window and verify Python has been installed with the following command ```python3 -V```
4. Verify pip3 has been installed with the following command ```pip3 -V```

## Install necessary packages
1. In a Terminal window, type the following command to install pipenv: ```pip3 install pipenv```

## Clone the git repository
1. In a Terminal window, change directory to where you want to clone the git repo folder in to: ```cd path/To/A/Folder```
2. Clone the git repo: ```git clone https://gitlab.com/chronicideas/selenium-scripts.git```
3. change directory in to the project folder: ```cd selenium-scripts```

## Running the tests
1. Start a pipenv shell: ```pipenv shell```
2. Install all the packages from the pipfile: ```pipenv install```

To run all the tests: ```pytest```

To run a single test script: ```pytest scripts/test_questions/test_question#.py``` (replace # with 1, 2 or 3)

To leave the pipenv shell: ```Ctrl + D```